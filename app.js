Vue.createApp({
    
    data() {
        return {
            title: 'Gestion des articles',
            articles: []
        }
    },

    methods: {

        formatDate(dateISO) {
            return new Date(dateISO).toLocaleDateString("fr-FR")
        },

        formatPrice(price) {
            return Number(price).toFixed(2) + ' €'
        }
    },

    created() {
        fetch('http://my-json-server.typicode.com/mathieuheliot/courses-api/books')
            .then( response => response.json() )
            .then( data => this.articles = data )
            .catch( error => console.error(error) )
    }

}).mount('#app')